FROM ubuntu:focal
VOLUME [ "/var/www/html" ]
RUN DEBIAN_FRONTEND=noninteractive apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install apache2 libapache2-mod-php7.4 php7.4 php7.4-mysql php7.4-gd php7.4-json php7.4-mbstring php7.4-curl php7.4-dom php7.4-sqlite php7.4-zip php7.4-intl php7.4-bcmath php7.4-soap zip libzip5 libzip-dev 
RUN a2enmod rewrite
COPY php.ini /etc/php/7.4/apache2/php.ini
COPY apache.conf /etc/apache2/site-skel.conf
# Esponiamo access e error in modo che stiano sui log dei container
RUN ln -sf /proc/self/fd/1 /var/log/apache2/access.log && ln -sf /proc/self/fd/1 /var/log/apache2/error.log
EXPOSE 80
CMD cp /etc/apache2/site-skel.conf /etc/apache2/sites-enabled/000-default.conf && sed -i 's@VARPHP_DOCUMENT_ROOT@'"${DOCUMENT_ROOT:-/var/www/html}"'@g' /etc/apache2/sites-enabled/000-default.conf && apachectl -D FOREGROUND
